package config

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

type Db struct {
	Url          string
	User         string
	Password     string
	Type         string
	MaxIdleConns int
	MaxOpenConns int
	LogMode      bool
}

type SysConfig struct {
	NoAuthPath    string
	NoAuthPathArr []string
}

type LogConfig struct {
	 LogLevel string
	 FilePath string
}

type Configuration struct {
	Db  Db
	Sys SysConfig
	Log LogConfig
}

// 定义一个cfg的变量待会初始化使用
var Cfg Configuration

// init 方法將會在被引用的時候自動執行
func init() {
	file, _ := os.Open("config/config.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	conf := Configuration{}
	err := decoder.Decode(&conf)
	if err != nil {
		fmt.Println("Error:", err)
	}
	if conf.Sys.NoAuthPath != "" {
		conf.Sys.NoAuthPathArr = strings.Split(conf.Sys.NoAuthPath, ",")
	}
	fmt.Println("config 的 init方法执行")
	Cfg = conf // 赋值给全局变量
}
