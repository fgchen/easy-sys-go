package entity

import (
	"database/sql/driver"
	"fmt"
	"time"
)

// JSONTime is alias type for time.JSONTime 用于返回格式化JSON时间
type JSONTime time.Time

const (
	timeFormart = "2006-01-02 15:04:05" // 固定写死的
	zone        = "Asia/Shanghai"       // 时区
)

// UnmarshalJSON implements json unmarshal interface.
func (t *JSONTime) UnmarshalJSON(data []byte) (err error) {
	if string(data) == "\"\"" {

	} else {
		now, err := time.ParseInLocation(`"`+timeFormart+`"`, string(data), time.Local)
		*t = JSONTime(now)
		return err
	}
	return
}

// MarshalJSON implements json marshal interface.
func (t JSONTime) MarshalJSON() ([]byte, error) {
	if time.Time(t).IsZero() {
		// 这里如果没有时间的话直接返回空字符的数组 需要加上双引号
		return []byte("\"\""), nil
	}
	b := make([]byte, 0, len(timeFormart)+2)
	b = append(b, '"')
	b = time.Time(t).AppendFormat(b, timeFormart)
	b = append(b, '"')
	return b, nil
}

func (t JSONTime) String() string {
	return time.Time(t).Format(timeFormart)
}

func (t JSONTime) local() time.Time {
	loc, _ := time.LoadLocation(zone)
	return time.Time(t).In(loc)
}

// Value ...
func (t JSONTime) Value() (driver.Value, error) {
	var zeroTime time.Time
	var ti = time.Time(t)
	if ti.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return ti, nil
}

// Scan valueof time.JSONTime 注意是指针类型 method
func (t *JSONTime) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = JSONTime(value)
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}
