package entity

// EasyPage 简单的分页对象
type EasyPage struct {
	Records interface{} `json:"records"`
	Total   int         `json:"total"`
	Size    int         `json:"size" form:"size"`
	Current int         `json:"current"  form:"current"`
}

// PageSize 获取每页数量
func (page *EasyPage) PageSize() int {
	if page.Size > defMaxSize {
		page.Size = defMaxSize
	} else {
		if page.Size < 1 {
			page.Size = 10
		}
	}
	return page.Size
}

// PageOffset 获取分页的偏移数量
func (page EasyPage) PageOffset() int {
	if page.Current < 1 {
		page.Current = 1
	}
	return page.PageSize() * (page.Current - 1)
}

var defMaxSize int = 1000
