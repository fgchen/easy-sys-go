package entity

// EasyMessage 返回数据时的统一对象
type EasyMessage struct {
	Code      int         `json:"code"`
	ErrorCode string      `json:"errorCode"`
	Message   string      `json:"message"`
	Data      interface{} `json:"data"`
}
