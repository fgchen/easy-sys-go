package dao

import (
	"easy-sys/config"
	"easy-sys/logger"
	"os"

	"github.com/jinzhu/gorm"
	// mysql 初始化默认数据库是mysql
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// Db 系统运行时，被依赖的时候执行，初始化数据库
var Db *gorm.DB

func init() {
	dbCfg := config.Cfg.Db
	if dbCfg.Type == "mysql" {

		if db, err := gorm.Open("mysql", dbCfg.User+":"+dbCfg.Password+dbCfg.Url); err != nil {
			logger.Log(dbCfg.User+":"+dbCfg.Password+dbCfg.Url, err)
			logger.Log("MySQL启动异常", err)
			os.Exit(0)
		} else {
			db = db
			db.DB().SetMaxIdleConns(dbCfg.MaxIdleConns)
			db.DB().SetMaxOpenConns(dbCfg.MaxOpenConns)
			db.LogMode(dbCfg.LogMode)
			Db = db
		}
	}
}
