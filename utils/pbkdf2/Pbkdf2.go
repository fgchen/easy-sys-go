package pbkdf2

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"golang.org/x/crypto/pbkdf2"
	"math/big"
)

const (
	iter       = 1000
	keyLen     = 64
	randomStr  = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
)

func EncryptPwdWithSalt(pwd, salt string) string {
	if pwd == "" || salt == "" {
		return ""
	}
	var pwdEn = pbkdf2.Key([]byte(pwd), []byte(salt), iter, keyLen, sha1.New)
	var res = hex.EncodeToString(pwdEn)
	return res
}

func CreateRandomString(len int) string  {
	var container string
	b := bytes.NewBufferString(randomStr)
	length := b.Len()
	bigInt := big.NewInt(int64(length))
	for i := 0;i < len ;i++  {
		randomInt,_ := rand.Int(rand.Reader,bigInt)
		container += string(randomStr[randomInt.Int64()])
	}
	return container
}