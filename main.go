package main

import (
	"easy-sys/server"
)

func main() {
	r := server.Server()
	// Listen and Server in 0.0.0.0:8080
	r.Run(":8888")

}
