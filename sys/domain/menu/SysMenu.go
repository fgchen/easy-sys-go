package menu

import "easy-sys/base/entity"

type SysMenu struct {
	MenuId         uint64    `json:"menuId"`
	MenuParentId   uint64    `json:"menuParentId"`
	MenuParentName string    `gorm:"-" json:"menuParentName"`
	MenuName       string    `json:"menuName"`
	MenuUrl        string    `json:"menuUrl"`
	MenuPath       string    `json:"menuPath"`
	MenuAuth       string    `json:"menuAuth"`
	MenuType       int       `json:"menuType"`
	MenuIcon       string    `json:"menuIcon"`
	MenuOrder      int       `json:"menuOrder"`
	Children       []SysMenu `gorm:"-" json:"children"`
	Open           bool      `gorm:"-" json:"open"`
	entity.BaseEntity
}

// 设置User的表名为`profiles`
func (SysMenu) TableName() string {
	return "sys_menu"
}
