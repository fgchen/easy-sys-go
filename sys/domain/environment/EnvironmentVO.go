package environment

import (
	"easy-sys/sys/domain/i18n"
	"easy-sys/sys/domain/menu"
)

type EnvironmentVO struct {
	I18n       map[string][]i18n.SysI18n `json:"i18n"`
	NoAuthMenu []menu.SysMenu            `json:"noAuthMenu"`
	LangList   []string                  `json:"langList"`
}
