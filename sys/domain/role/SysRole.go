package role

import "easy-sys/base/entity"

type SysRole struct {
	RoleId   uint64   `json:"roleId"`
	RoleName string   `json:"roleName"`
	RoleDesc string   `json:"roleDesc"`
	Auths    []uint64 `gorm:"-" json:"auths"`
	UserIds  []uint64 `gorm:"-" json:"userIds"`
	entity.BaseEntity
}

// 设置User的表名为`profiles`
func (SysRole) TableName() string {
	return "sys_role"
}
