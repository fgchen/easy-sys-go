package role

type SysRoleMenu struct {
	RoleId uint64
	MenuId uint64
}

// 设置User的表名为`profiles`
func (SysRoleMenu) TableName() string {
	return "sys_role_menu"
}
