package menu

import (
	"easy-sys/sys/service/menu"

	"github.com/gin-gonic/gin"
)

func Routes(route *gin.RouterGroup) {
	menuRoutes := route.Group("/menu")
	menuRoutes.POST("all", menu.All)
	menuRoutes.POST("getRoleMenu", menu.GetRoleMenu)
	menuRoutes.POST("list", menu.List)
	menuRoutes.POST("save", menu.Save)
}
