package role

import (
	"easy-sys/server/middleware/permission"
	roleService "easy-sys/sys/service/role"

	"github.com/gin-gonic/gin"
)

// Routes 角色路由初始化
func Routes(route *gin.RouterGroup) {
	roleRoutes := route.Group("/role")
	roleRoutes.POST("list", permission.RouterHasPermission("sys.role.list"), roleService.List)
	roleRoutes.POST("saveRoleAuth", roleService.SaveRoleAuth)
	roleRoutes.POST("addRoleUser", roleService.AddRoleUser)
	roleRoutes.POST("deleteRoleUserByRole", roleService.DeleteRoleUserByRole)
	roleRoutes.POST("delete", roleService.Delete)
	roleRoutes.POST("save", roleService.Save)
}
