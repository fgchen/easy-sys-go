package user

import (
	"easy-sys/sys/service/user"

	"github.com/gin-gonic/gin"
)

// Routes 用户路由.
func Routes(route *gin.RouterGroup) {
	userRoutes := route.Group("/user")
	userRoutes.POST("login", user.Login)
	userRoutes.GET("info", user.Info)
	userRoutes.POST("getUserByRole", user.GetUserByRole)
	userRoutes.POST("list", user.List)
	userRoutes.POST("save", user.Save)
	userRoutes.POST("delete", user.Delete)
	userRoutes.POST("logout", user.Logout)
}
