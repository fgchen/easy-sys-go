package i18n

import (
	"easy-sys/sys/service/i18n"

	"github.com/gin-gonic/gin"
)

func Routes(route *gin.RouterGroup) {
	i18nRoutes := route.Group("/i18n")
	i18nRoutes.POST("list", i18n.List)
	i18nRoutes.POST("save", i18n.Save)
}
