package environment

import (
	"easy-sys/sys/domain/environment"
	"easy-sys/sys/domain/i18n"
	"easy-sys/sys/domain/menu"
	"easy-sys/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetEnvInfo 获取系统初始化信息
func GetEnvInfo(context *gin.Context) {
	var env = environment.EnvironmentVO{}
	var zhCN = "zh-CN"
	var enUS = "en-US"
	env.LangList = []string{zhCN, enUS}

	m := map[string][]i18n.SysI18n{}
	m[zhCN] = i18n.GetAllI18nByLang(zhCN)
	m[enUS] = i18n.GetAllI18nByLang(enUS)

	env.I18n = m

	env.NoAuthMenu = menu.GetMenuNoAuth()

	context.JSON(http.StatusOK, utils.GetSuccessMessage(env))
}
